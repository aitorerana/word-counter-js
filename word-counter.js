setInterval(function() {
    $('.word-counter').each(function () {
        var inputId = $(this).data('id');
        $(this).html($('#' + inputId).val().length);
    });
}, 100);