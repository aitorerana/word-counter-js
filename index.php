<!DOCTYPE html>
    <html>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>word-counter</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <input type="text" id="input-id" />
        <div class="word-counter" data-id="input-id"></div>
        <script src="word-counter.js"></script>
    </body>
</html>