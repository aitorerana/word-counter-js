# word-counter #

This is jQuery library to count and display inputs word length.

### Requirements ###

This library requires to import **[jQuery](https://jquery.com/ "jQuery")** before calling the function.

### Configuration ###

1. Import the script: ```<script src="word-counter.js"></script>```
2. Put your code un your HTML: ```<div class="word-counter" data-id="input"></div>```

### Parameters ###

* **data-id**: The id of the input you want to count words. Required.

### Run the project ###

Here a simple command to start this project localy: **php -S localhost:7000**

### Run the project with Docker ###

Run the command: **docker-compose up -d**

### Author ###

[Aitor Eraña](http://cv.aitorerana.com/ "CV aitorerana")